TEXPROGRAM=xelatex
TEXFLAGS=
SPELLPROGRAM=aspell
ASPELLWORDS=wordlist
SPELLFLAGS=--lang=ru --mode=tex --personal=./$(ASPELLWORDS)
FILE=main
SOURCES=$(FILE).tex
OBJECTS=
EXECUTABLE=$(FILE).pdf
CLEANFILES=*.aux *.blg *.out *.bbl chapters/*.bak *.bak *.log *.dvi *.toc *.synctex.gz img/*.pdf $(ASPELLWORDS) 

.FORCE: $(EXECUTABLE)

.PHONY: clean read

all: $(SOURCES) $(EXECUTABLE)

pdf:

	@if [ ! -f $(PWD)/wordlist ] ;\
	then\
		cp -v $(PWD)/wordlist.nd $(PWD)/wordlist;\
	fi;

	@for file in `find $(PWD)/chapters/*.tex -type f` ;\
	do\
		echo -e "[$(GREEN_COLOR)check spell in $${file}$(RESET_COLOR)]" ;\
		$(SPELLPROGRAM) $(SPELLFLAGS) check $${file} ;\
	done;
	$(TEXPROGRAM) $(TEXFLAGS) $(FILE)
	$(TEXPROGRAM) $(TEXFLAGS) $(FILE)

clean:
	rm -f $(CLEANFILES)