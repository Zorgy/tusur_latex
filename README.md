Требования
----------
1. texlive-full
2. aspell-ru
3. Шрифты (Приложены)
3. VS code или другой редактор

Настройка
----------

Для настройки необходимо скопировать содержимое папки Fonts в директорию `/usr/share/fonts/TTF`. В итоге команда `fc-list | grep TimesNewRoman` должна выводить 4 шрифта.

Компиляция
----------

Все текущие отчёты компилируются с помощью `xelatex` (входит в состав texlive-full).

Для компиляции необходимо выполнить `make pdf` из директории с проектом.
